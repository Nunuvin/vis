/*
avaliable under gpl v3 
ball collision

*/

int scrX = 800;
int scrY = 600;





/*
  find distance between 2 dots
*/
float delta(float x1, float y1, float x2, float y2){
   float dx = abs(x1 - x2);
   float dy = abs(y1 - y2);
   return sqrt(dx*dx+dy*dy);
  
}

/* ball class aka elipse */
class El{
     float x,y;
     int r;
     int wc;

     float xspeed = 1.4;  // Speed of the shape
     float yspeed = 1.1;  // Speed of the shape
     El(){}; //empty constructor
     
     //set var
     void set(float x, float y, int r){
       this.x = x;
       this.y = y;
       this.r = r;
       wc =millis();

     }
      
      //process move
     void move(){
      x = x + (xspeed);
      y = y + (yspeed);
     }
     
     //overlap with another ellipse?
     void objCollision(El e2){
       if (delta(this.x, this.y, e2.x, e2.y)<(this.r+e2.r)){
          //collision!!!
          float xspd = e2.xspeed;
          float yspd = e2.yspeed;
          
          e2.xspeed = this.xspeed;
          e2.yspeed = this.yspeed;
          
          this.xspeed = xspd;
          this.yspeed = yspd;
          
       }
    }
    
    //collided with wall?
    void wallCollision(){
      if (wc+7 < millis()){ //pause letting obj move away from wall instead of getting stuck
        if (this.x > width-this.r || this.x < this.r) {
          this.xspeed *= -1;
        }
        if (this.y > height-this.r || this.y < this.r) {
          this.yspeed *= -1;
        }
          wc = millis();
        }
    }
    
    //draw this obj
    void draw(){
      ellipse(this.x, this.y, this.r, this.r);
    }
    
    //update current position check for wall collision
    void update(){
     this.move();
     this.wallCollision();
    }
}

int r = 20;
ArrayList<El> eList = new ArrayList<El>();

//restart sim with new ball pos and new # of balls
void resetList(){
  float x1 = 0, y1 = 0;
  int listSize = floor(random(3,11)); //float is weird sometimes 2 is floored to 1 :/
  
  if(!eList.isEmpty()){
    eList.clear();     
  }
  
  for(int i=0; i<listSize;i++){
    boolean invalid = true;
    while(invalid){
      invalid = false;
      x1 = random(r, scrX-r);
      y1 = random(r, scrY-r);
      for(int j=0; j<eList.size();j++){
          if( delta(x1,y1,eList.get(j).x,eList.get(j).y)<=2*r){
             invalid = true;
             break;
          }
      }
    }
    El e = new El();
    e.set(x1,y1,r);
    eList.add(e);
    
  }

}

void setup() 
{

  size(800, 600);
  noStroke();
  frameRate(120);
  ellipseMode(RADIUS);
  randomSeed(60*(60*hour()+minute())+second());
  
  resetList();
}

void draw() 
{
  background(102);

     for(int i=0; i<eList.size()-1;i++){
       eList.get(i).update();
        for(int j=i+1; j<eList.size();j++){
          eList.get(i).objCollision(eList.get(j));
        } 
       eList.get(i).draw();
     }

}

void keyPressed(){
   if(key==' '){ // rest on space
      resetList();
   }
}
